package es.upm.emse.absd.team0.behaviours;

import static org.junit.jupiter.api.Assertions.*;

import es.upm.emse.absd.team0.agents.tribe.behaviours.AnswerBehaviour;
import org.junit.jupiter.api.Test;

class AnswerBehaviourTest {

    @Test void testSalute() {
        assertFalse(new AnswerBehaviour(null, 1).salute().isEmpty());
    }

}