package es.upm.emse.absd;

import static es.upm.emse.absd.Utils.*;

import es.upm.emse.absd.team0.agents.platform.AgPlatform;
import es.upm.emse.absd.team0.agents.tribe.AgTribe;

import jade.Boot;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.StaleProxyException;

/**
 * @author      Jose Maria Barambones <j.barambones@upm.es>
 * @version     1.2.1
 *
 * Main Class.
 */
public class Main {

    private static jade.wrapper.AgentContainer cc;
    private static jade.core.Runtime rt;

    // Executed from Singletons.
    private static void loadBoot(){

        // Load JADE with GUI for debugging.
        Boot.main(new String[] {"-gui"});

        // Create a default profile
        rt = jade.core.Runtime.instance();

        System.out.println("Agent Containers created...");

    }

    private static void loadMyPlatformAgents() {
        // now set the profiles to start the containers
        ProfileImpl agentContainerProfile = new ProfileImpl(null, 1200, null);
        agentContainerProfile.setParameter(Profile.CONTAINER_NAME, "Platform-Container");
        cc = rt.createAgentContainer(agentContainerProfile);

        try {
            cc.createNewAgent("Link", AgPlatform.class.getName(), new Object[]{"0"}).start();
        } catch (StaleProxyException e) {
            System.err.println("Error creating platform agents!!!");
            e.printStackTrace();
            System.exit(1);
        }
    }
    private static void loadMyTribeAgents() {
        // now set the profiles to start the containers
        ProfileImpl agentContainerProfile = new ProfileImpl(null, 1200, null);
        agentContainerProfile.setParameter(Profile.CONTAINER_NAME, TRIBE_0);
        cc = rt.createAgentContainer(agentContainerProfile);

        try {
            cc.createNewAgent("Navi", AgTribe.class.getName(), new Object[]{"0"}).start();
        } catch (StaleProxyException e) {
            System.err.println("Error creating my tribe agents!!!");
            e.printStackTrace();
            System.exit(1);
        }
    }

/*
    private static void loadTribe{N}Agents() {
        // now set the profiles to start the containers
        ProfileImpl agentContainerProfile = new ProfileImpl(null, 1200, null);
        agentContainerProfile.setParameter(Profile.CONTAINER_NAME, TRIBE_N);
        cc = rt.createAgentContainer(agentContainerProfile);

        try {
            cc.createNewAgent("Taya", es.upm.emse.absd.team{N}.agents.tribe.AgTribe.class.getName(), new Object[]{"0"}).start();
        } catch (StaleProxyException e) {
            System.err.println("Error creating agents from tribe Demo!!!");
            e.printStackTrace();
            System.exit(1);
        }
    }
//*/

    private static void printHelp() {
        System.out.println("""
            --------------------------------------
            --- EMSE-ABSD JADE World of Agents ---
            --------------------------------------
            Agents:
                + Platform:
                    - AgPlatform: An agent that responds when its called by its name.
                + Tribe:
                    - AgTribe: An agent that try to talk with his/her colleague.
            Usage: java -jar woa.jar [options]
                + options:
                    -h, --help  Prints help
                    -d, -debug  Run JADE agents from the compilation
                    -b, -build  Run agents from generated jars
            """
        );
    }

    public static void main(String[] args) {

        System.setProperty("java.util.logging.SimpleFormatter.format", ANSI_WHITE + "[%1$tF %1$tT] [%4$-7s] %5$s %n");

        if (args.length == 0 || args[0].equals("-h") || args[0].equals("--help")) {
            printHelp();
            System.exit(0);
        }

        System.out.println("Starting...");
        loadBoot();
        loadMyPlatformAgents();

        if (args[0].equals("-d") || args[0].equals("-debug")) {
            loadMyTribeAgents();
        } else if (args[0].equals("-b") || args[0].equals("-build")) {
            //loadTribe1Agents();
            //loadTribe2Agents();
            //loadTribe3Agents();
            //loadTribe4Agents();
        }

        System.out.println("MAS loaded...");

    }

}
