package es.upm.emse.absd.team0.agents.platform;

import static es.upm.emse.absd.Utils.*;

import es.upm.emse.absd.Utils;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import lombok.extern.java.Log;

import java.io.Serializable;

@Log
public class AgPlatform extends Agent {

    protected void setup() {
        log.info(ANSI_CYAN + this.getLocalName() + ": I'm alive!!!");

        addBehaviour(new CyclicBehaviour() {
            @Override public void action() {
                ACLMessage msg = this.getAgent().blockingReceive();
                if (msg.getContent().contains(this.getAgent().getLocalName())) {
                    log.severe(ANSI_YELLOW + this.getAgent().getLocalName() + " (to " + msg.getSender().getLocalName() +") : WHAT!!!");
                    this.getAgent().send(Utils.newMsg(this.getAgent(), ACLMessage.INFORM, "WHAT!!!", msg.getSender()));
                }
            }
        });
    }
}
