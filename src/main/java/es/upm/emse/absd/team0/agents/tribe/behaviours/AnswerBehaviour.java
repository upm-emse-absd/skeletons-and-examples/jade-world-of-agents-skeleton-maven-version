package es.upm.emse.absd.team0.agents.tribe.behaviours;

import static es.upm.emse.absd.Utils.ANSI_CYAN;

import es.upm.emse.absd.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Log
public class AnswerBehaviour extends TickerBehaviour {

    List<String> words = Arrays.asList("Hey!", "Listen!", "Hello!", "Link!");
    Random rand = new Random();

    public AnswerBehaviour(Agent a, long period) {
        super(a, period);
    }

    @Override protected void onTick() {

        ACLMessage msg = this.getAgent().receive();
        if (msg != null) {
            log.info(ANSI_CYAN + this.getAgent().getLocalName() + ": HIIIIIIII!!! :D");
            this.getAgent().doSuspend();
        } else {
            String randomWord = salute();
            log.warning(ANSI_CYAN + this.getAgent().getLocalName() + ": " + randomWord);
            this.getAgent().send(Utils.newMsg(this.getAgent(), ACLMessage.INFORM, randomWord,
                new AID("Link", AID.ISLOCALNAME)));
        }

    }

    /**
     * @return A random salutation
     */
    public String salute() {
        return words.get(rand.nextInt(words.size()));
    }

}
