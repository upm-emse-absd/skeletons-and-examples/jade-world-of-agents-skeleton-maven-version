package es.upm.emse.absd.team0.agents.tribe;

import static es.upm.emse.absd.Utils.ANSI_CYAN;

import es.upm.emse.absd.team0.agents.tribe.behaviours.AnswerBehaviour;
import jade.core.Agent;
import lombok.extern.java.Log;

@Log
public class AgTribe extends Agent {

    protected void setup()
    {
        log.info(ANSI_CYAN + this.getLocalName() + ": I'm alive!!!");
        addBehaviour(new AnswerBehaviour(this,2000));
    }

}

